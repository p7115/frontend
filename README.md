# ProjetDevWeb (Frontend)

Repository du frontend de l'application.
Ecrit avec Angular, en utilisant également :
- [Bootstrap](https://getbootstrap.com/) (Customisé avec SASS)
- [NgBootstrap](https://ng-bootstrap.github.io/)
- [Fontawesome](https://fontawesome.com/)

## Serveur de développement

Exécuter `ng serve`. Accès à l'adresse http://localhost:4200/.

## Build

Exécuter `ng build`. Build disponible dans le dossier `dist/`.

## Docker

- Bien penser à build le projet avant ! (Voir point précédent).
- Exécuter `docker build -t ossacipe/projetdevweb-frontend .` pour construire l'image.
- Pour lancer un conteneur avec l'image, exécuter `docker run IMAGE ossacipe/projetdevweb-frontend`.
- Accès à l'URL http://localhost:8080/.

## DockerHub

[Accès ici](https://hub.docker.com/r/ossacipe/projetdevweb-frontend)
