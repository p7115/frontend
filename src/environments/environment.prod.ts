export const environment = {
  production: true,
  backend: {
    protocol: 'http',
    host: '127.0.0.1',
    port: '8081',
    endpoints: {
      nomenclature: {
        all: '/nomenclature/'
      },
      tva: {
        all: '/tva/'
      },
      produit: {
        all: '/produit/',
        byId: '/produit/:id',
        byLibelle: '/produit/libelle/:libelle'
      }
    }
  }
};
