import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModaleProduitComponent } from './modale-produit.component';

describe('ModaleProduitComponent', () => {
  let component: ModaleProduitComponent;
  let fixture: ComponentFixture<ModaleProduitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModaleProduitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModaleProduitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
