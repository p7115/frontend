import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {faInfoCircle, faSave, faUndo} from "@fortawesome/free-solid-svg-icons";
import {Produit} from "../models/Produit";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {Mode} from "../models/Mode";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Nomenclature} from "../models/Nomenclature";
import {Tva} from "../models/Tva";
import {ProduitService} from "../services/produit.service";
import {NomenclatureService} from "../services/nomenclature.service";
import {TvaService} from "../services/tva.service";

@Component({
  selector: 'app-modale-produit',
  templateUrl: './modale-produit.component.html',
  styleUrls: ['./modale-produit.component.scss']
})
export class ModaleProduitComponent implements OnInit {

  @Input()
  produit?: Produit;

  @Input()
  mode?: Mode;

  lstNomenclature: Nomenclature[] = [];
  lstTva: Tva[] = [];
  form: any;
  estValide: boolean;
  titre: string;

  @Output()
  produitCreated: EventEmitter<Produit>;

  @Output()
  produitUpdated: EventEmitter<Produit>;

  // Import des icones utilisées dans la vue
  faSave = faSave;
  faUndo = faUndo;
  faInfoCircle = faInfoCircle;

  constructor(public activeModal: NgbActiveModal,
              private readonly nomenclatureService: NomenclatureService,
              private readonly tvaService: TvaService,
              private readonly produitService: ProduitService) {

    this.titre = "";
    this.estValide = true;
    this.produitCreated = new EventEmitter<Produit>();
    this.produitUpdated = new EventEmitter<Produit>();
    this.form = new FormGroup({
      id: new FormControl(null),
      libelle: new FormControl('', Validators.compose([Validators.required, Validators.minLength(2)])),
      ean: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]{12}')])),
      idNomenclature: new FormControl('', Validators.compose([Validators.required, Validators.min(0)])),
      marque: new FormControl('', Validators.compose([Validators.required, Validators.minLength(2)])),
      fournisseur: new FormControl('', Validators.compose([Validators.required, Validators.minLength(2)])),
      idTva: new FormControl('', Validators.compose([Validators.required, Validators.min(0)])),
      stock: new FormControl('', Validators.compose([Validators.required, Validators.min(0), Validators.pattern('^[0-9]*$')])),
      prix: new FormControl('', Validators.compose([Validators.required, Validators.min(0), Validators.pattern('^[0-9]+(.[0-9]{1,2})?$')]))
    });
  }

  ngOnInit(): void {

    this.titre = this.mode == Mode.Creation ? "Création" : "Modification";

    // Copie des valeurs du produit dans le form
    this.form.setValue({
      id: this.produit ? this.produit.id : null,
      libelle: this.produit ? this.produit.libelle : '',
      ean: this.produit ? this.produit.ean : '',
      idNomenclature: this.produit ? this.produit.nomenclature?.id : -1,
      marque: this.produit ? this.produit.marque : '',
      fournisseur: this.produit ? this.produit.fournisseur : '',
      idTva: this.produit ? this.produit.tva?.id : -1,
      stock: this.produit ? this.produit.stock : '',
      prix: this.produit ? this.produit.prix : ''
    });

    // Init de la liste de Nomenclature
    this.nomenclatureService.getAll().subscribe({
      next : (nmc) => this.lstNomenclature = nmc,
      error : (err) => alert("Une erreur est survenue lors de l'appel à l'API : " + err)
    });

    // Init de la liste de TVA
    this.tvaService.getAll().subscribe({
      next : (tva) => this.lstTva = tva,
      error : (err) => alert("Une erreur est survenue lors de l'appel à l'API : " + err)
    });

  }

  // Méthode de sauvegarde
  save(){
    this.estValide = this.form.valid;
    if(this.estValide){
      if(this.mode == Mode.Creation){
        this.create();
      }else if(this.mode == Mode.Edition){
        this.update();
      }
      this.activeModal.close();
    }
  }

  // Création d'un produit
  create(){
    this.produitService.create(this.form.value).subscribe({
      next : (prd) => this.produitCreated.emit(prd),
      error : (err) => alert("Une erreur est survenue lors de l'appel à l'API : " + err)
    });
  }

  // Màj d'un produit
  update(){
    this.produitService.update(this.form.value).subscribe({
      next : (prd) => this.produitUpdated.emit(prd),
      error : (err) => alert("Une erreur est survenue lors de l'appel à l'API : " + err)
    });
  }

}
