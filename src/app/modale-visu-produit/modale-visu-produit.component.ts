import {Component, Input, OnInit} from '@angular/core';
import {Produit} from "../models/Produit";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-modale-visu-produit',
  templateUrl: './modale-visu-produit.component.html',
  styleUrls: ['./modale-visu-produit.component.scss']
})
export class ModaleVisuProduitComponent implements OnInit {

  @Input()
  produit?: Produit;

  // Icone utilisée dans la vue
  faTimesCircle = faTimesCircle;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit(): void {}

}
