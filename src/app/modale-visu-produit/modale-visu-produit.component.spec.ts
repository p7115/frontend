import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModaleVisuProduitComponent } from './modale-visu-produit.component';

describe('ModaleVisuProduitComponent', () => {
  let component: ModaleVisuProduitComponent;
  let fixture: ComponentFixture<ModaleVisuProduitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModaleVisuProduitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModaleVisuProduitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
