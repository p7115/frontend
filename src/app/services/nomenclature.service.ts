import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {Nomenclature} from "../models/Nomenclature";

@Injectable({
  providedIn: 'root'
})
export class NomenclatureService {

  private urlServer:any = {};

  constructor(private readonly http: HttpClient) {
    let baseUrl = `${environment.backend.protocol}://${environment.backend.host}`;
    if (environment.backend.port) {
      baseUrl += `:${environment.backend.port}`;
    }
    Object.keys(environment.backend.endpoints.nomenclature).forEach(
      // @ts-ignore
      k => (this.urlServer[k] = `${baseUrl}${environment.backend.endpoints.nomenclature[k]}`)
    );
  }

  // Récupére toutes les nomenclatures
  getAll(): Observable<Nomenclature[]> {
    return this.http.get<Nomenclature[]>(this.urlServer.all);
  }
}
