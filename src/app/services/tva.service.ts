import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {Tva} from "../models/Tva";

@Injectable({
  providedIn: 'root'
})
export class TvaService {

  private urlServer:any = {};

  constructor(private readonly http: HttpClient) {
    let baseUrl = `${environment.backend.protocol}://${environment.backend.host}`;
    if (environment.backend.port) {
      baseUrl += `:${environment.backend.port}`;
    }
    Object.keys(environment.backend.endpoints.tva).forEach(
      // @ts-ignore
      k => (this.urlServer[k] = `${baseUrl}${environment.backend.endpoints.tva[k]}`)
    );
  }

  // Récupére toutes les TVA
  getAll(): Observable<Tva[]> {
    return this.http.get<Tva[]>(this.urlServer.all);
  }

}
