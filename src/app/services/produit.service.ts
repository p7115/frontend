import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Produit} from "../models/Produit";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ProduitService {

  private urlServer:any = {};

  constructor(private readonly http: HttpClient) {
    let baseUrl = `${environment.backend.protocol}://${environment.backend.host}`;
    if (environment.backend.port) {
      baseUrl += `:${environment.backend.port}`;
    }
    Object.keys(environment.backend.endpoints.produit).forEach(
      // @ts-ignore
      k => (this.urlServer[k] = `${baseUrl}${environment.backend.endpoints.produit[k]}`)
    );
  }

  // Récupére tout les produits
  getAll(): Observable<Produit[]> {
    return this.http.get<Produit[]>(this.urlServer.all);
  }

  // Récupére un produit par son ID
  getById(id: number): Observable<Produit> {
    return this.http.get<Produit>(this.urlServer.byId.replace(':id', id));
  }

  // Récupére les produits ayant le libellé spécifié
  getByLibelle(libelle: string): Observable<Produit[]> {
    return this.http.get<Produit[]>(this.urlServer.byLibelle.replace(':libelle', libelle));
  }

  // Supprime le produit
  delete(produit: Produit): Observable<any> {
    return this.http.delete(this.urlServer.byId.replace(':id', produit.id));
  }

  // Crée un produit
  create(produit: Produit): Observable<Produit> {
    return this.http.post<Produit>(this.urlServer.all, produit);
  }

  // Màj d'un produit
  update(produit: Produit): Observable<Produit> {
    return this.http.put<Produit>(this.urlServer.byId.replace(':id', produit.id), produit);
  }
}
