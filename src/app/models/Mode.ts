export enum Mode {
  Visualisation = "Check",
  Edition = "Edit",
  Creation = "Create"
}
