import {Nomenclature} from "./Nomenclature";
import {Tva} from "./Tva";

export interface Produit {
  id?: number;
  libelle?: string;
  ean?: string;
  nomenclature?: Nomenclature;
  marque?: string;
  fournisseur?: string;
  tva?: Tva;
  stock?: number;
  prix?: number;
}
