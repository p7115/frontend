import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {faCheckCircle, faBan} from "@fortawesome/free-solid-svg-icons";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {Produit} from "../models/Produit";

@Component({
  selector: 'app-modale-confirm',
  templateUrl: './modale-confirm.component.html',
  styleUrls: ['./modale-confirm.component.scss']
})
export class ModaleConfirmComponent implements OnInit {

  @Input()
  text: string;

  @Output()
  accepted: EventEmitter<string>;

  // Icones utilisées dans la vue
  faBan = faBan;
  faCheckCircle = faCheckCircle;

  constructor(public activeModal: NgbActiveModal) {
    this.text = "";
    this.accepted = new EventEmitter<string>();
  }

  ngOnInit(): void {
  }

  // Si l'utilisateur clique sur le bouton oui
  confirm() {
    this.accepted.emit("accepted");
    this.activeModal.close();
  }
}
