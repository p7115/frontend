import { Component, OnInit } from '@angular/core';
import {faExclamationCircle} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent implements OnInit {

  // Icone utilisée dans la vue
  faExclamationCircle = faExclamationCircle;

  constructor() {
  }

  ngOnInit(): void {
  }

}
