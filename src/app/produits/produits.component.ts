import {Component, ComponentRef, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Produit} from "../models/Produit";
import {ProduitService} from "../services/produit.service";
import {faPlusCircle, faSearch, faEdit, faTimesCircle} from "@fortawesome/free-solid-svg-icons";
import {Mode} from "../models/Mode";
import {ModaleProduitComponent} from "../modale-produit/modale-produit.component";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ModaleVisuProduitComponent} from "../modale-visu-produit/modale-visu-produit.component";
import {ModaleConfirmComponent} from "../modale-confirm/modale-confirm.component";

@Component({
  selector: 'app-produits',
  templateUrl: './produits.component.html',
  styleUrls: ['./produits.component.scss']
})
export class ProduitsComponent implements OnInit {

  lstProduits: Produit[] = [];
  modeSelected: Mode;
  produitSelected: Produit | null;
  Mode = Mode;
  form: any;

  @ViewChild(ModaleProduitComponent)
  modale: any;

  // Import des icones utilisées
  faPlusCircle = faPlusCircle;
  faSearch = faSearch;
  faEdit = faEdit;
  faTimesCircle = faTimesCircle;

  constructor(private readonly produitService: ProduitService, private modalService : NgbModal) {
    this.modeSelected = Mode.Creation;
    this.produitSelected = null;
    this.form = new FormGroup({
      libelle: new FormControl('')
    });
  }

  // Au chargement, on charge tous les produits
  ngOnInit(): void {
    this.loadAllProduit();
  }

  // Ouverture de la modale pour Création / Edition
  openModaleProduit(produit: Produit | null, mode: Mode) {

    this.modeSelected = mode;
    this.produitSelected = produit;

    const modalRef = this.modalService.open(ModaleProduitComponent, {scrollable: true, backdrop: "static"});
    modalRef.componentInstance.mode = mode;
    modalRef.componentInstance.produit = produit;
    modalRef.componentInstance.produitCreated.subscribe(
      (newProduit: Produit) => this.addProduit(newProduit)
    )
    modalRef.componentInstance.produitUpdated.subscribe(
      (updatedProduit: Produit) => this.updateProduit(updatedProduit)
    )
  }

  // Ouverture de la modale pour Visualisation
  openModaleVisuProduit(produit: Produit) {
    this.modeSelected = Mode.Visualisation;
    this.produitSelected = produit;
    const modalRef = this.modalService.open(ModaleVisuProduitComponent, {scrollable: true, backdrop:"static"});
    modalRef.componentInstance.produit = produit;
  }

  // Ouverture de la modale de confirmation avant suppression
  openModaleConfirmDelete(produit: Produit){
    const modalRef = this.modalService.open(ModaleConfirmComponent, {backdrop: "static"});
    modalRef.componentInstance.text = "Êtes-vous sûr de vouloir supprimer ce produit ?";
    modalRef.componentInstance.accepted.subscribe(
      (msg: string) => this.delete(produit)
    )
  }

  // Suppression d'un produit
  delete(produit: Produit){
    this.produitService.delete(produit).subscribe({
      next : (rep) => this.lstProduits = this.lstProduits.filter(item => item.id != produit.id),
      error : (err) => alert("Une erreur est survenue lors de l'appel à l'API : " + err)
    });
  }

  // Ajoute le produit crée dans la modale
  addProduit(produit: Produit){
    this.lstProduits.unshift(produit);
  }

  // Met à jour le produit sélectionné avec le résultat de la modale
  updateProduit(produit: Produit){
    Object.assign(this.produitSelected, produit);
  }

  // Charge tous les produits
  loadAllProduit(){
    this.produitService.getAll().subscribe({
      next : (rep) => this.lstProduits = rep,
      error : (err) => alert("Une erreur est survenue lors de l'appel à l'API : " + err)
    });
  }

  // Recherche des produits avec critères de recherche
  searchProduit(){
    if(this.form.value.libelle==''){
      this.loadAllProduit();
    } else {
      this.produitService.getByLibelle(this.form.value.libelle).subscribe({
        next : (rep) => this.lstProduits = rep,
        error : (err) => alert("Une erreur est survenue lors de l'appel à l'API : " + err)
      });
    }
  }
}
