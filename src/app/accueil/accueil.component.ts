import { Component, OnInit } from '@angular/core';
import {faAngleRight} from "@fortawesome/free-solid-svg-icons";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit {
  faAngleRight = faAngleRight;

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
  }

  goToProduits($event: MouseEvent){
    this.router.navigate(['produits']);
  }

}
