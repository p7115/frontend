FROM nginx:alpine
COPY dist/projet-dev-web/ /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
